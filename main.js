const injectStyles = () => {
  const css = `.sr-only {
    position: absolute;
    position: absolute !important;
    width: 1px !important;
    height: 1px !important;
    padding: 0 !important;
    margin: -1px !important;
    overflow: hidden !important;
    clip: rect(0,0,0,0) !important;
    white-space: nowrap !important;
    border: 0 !important;
  }
  .sr-only-highlight {
    background-color: yellow;
    color: red;
  }
  `;


  const head = document.head || document.getElementsByTagName('head')[0];
  const style = document.createElement('style');

  style.type = 'text/css';
  if (style.styleSheet){
    // This is required for IE8 and below.
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }

  head.appendChild(style);
};


const matchDomain = (url, domain) => {
  try {
    const parsedUrl = new URL(url);
    return parsedUrl.hostname === domain;
  } catch (e) {
    return false;
  }
};


const augmentLink = (linkElement, screenReaderClass = 'sr-only', screenReaderText = null) => {
  // Wrap existing text in a span that's visible but hidden from screen readers
  const visualSpan = document.createElement('span');
  visualSpan.innerHTML = linkElement.innerHTML;
  visualSpan.setAttribute('aria-hidden', 'true');
  
  // Create a new span for screen reader text
  const screenReaderSpan = document.createElement('span');
  screenReaderSpan.innerHTML = screenReaderText || 'Screen Reader Text'; // Can use custom or default text for demo
  screenReaderSpan.classList.add(screenReaderClass);

  // Clear original link text and append new spans
  linkElement.innerHTML = '';
  linkElement.appendChild(visualSpan);
  linkElement.appendChild(screenReaderSpan);
};


const replaceLink = (linkElement, screenReaderClass = 'sr-only', screenReaderText = null) => {
  const parent = linkElement.parentNode;
  const originalHref = linkElement.href;
  
  // Link for sighted users
  const visualLink = document.createElement('a');
  visualLink.href = originalHref;
  visualLink.innerHTML = linkElement.innerHTML;
  visualLink.setAttribute('aria-hidden', 'true'); // Hide from screen readers
  
  // Link for screen readers
  const screenReaderLink = document.createElement('a');
  screenReaderLink.href = originalHref;
  screenReaderLink.innerHTML = screenReaderText || linkElement.innerHTML; // Use custom or default text
  screenReaderLink.classList.add(screenReaderClass); // Use custom or default class

  parent.insertBefore(visualLink, linkElement);
  parent.insertBefore(screenReaderLink, linkElement);
  
  parent.removeChild(linkElement);
};


async function fetchDoiTitle(doi) {
  try {
    const res = await fetch(`https://api.crossref.org/works/${encodeURIComponent(doi)}`);
    if (!res.ok) {
      return "Custom Screen Reader Text";
    }
    const json = await res.json();
    return json.message.title[0];
  } catch (error) {
    return "Custom Screen Reader Text";
  }
}

function extractDoi(url) {
  const doiPrefix = 'https://doi.org/';
  if (url.startsWith(doiPrefix)) {
    return url.substring(doiPrefix.length);
  }
  return null;
}

async function processLinks(targetDomain, srClass = 'sr-only') {
  Array.from(document.querySelectorAll("a")).forEach(async (link) => {
    if (matchDomain(link.href, targetDomain)) {
      const doi = extractDoi(link.href);  // Implement this function to extract DOI from the href
      const title = await fetchDoiTitle(doi);
      augmentLink(link, srClass, title);
    }
  });
}


const main = () => {
  injectStyles();
  const domainToMatch = 'doi.org';
  const customClassName = 'sr-only';
  const customText = 'Custom Screen Reader Text';
  processLinks(domainToMatch, customClassName, customText);
}


document.addEventListener('DOMContentLoaded', () => {
  if (document.readyState === "loading") {
    // If the document is still loading, add an event listener
    document.addEventListener("DOMContentLoaded", main);
  } else {
    // Otherwise, run the main function since DOM is ready
    main();
  }
});