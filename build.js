const fs = require('fs');
const rimraf = require('rimraf')

// Determine the base URL
const baseURL = process.env.CI_PAGES_URL || 'http://localhost:8080';

// Set the script location
const scriptURL = `${baseURL}/main.js`

// Generate bookmarklet
const bookmarkletCode = `
javascript:(function() {
  var script = document.createElement('script');
  script.onload = function() {
    main();
  };
  script.src = '${scriptURL}';
  document.body.appendChild(script);
})();
`;

// Generate instructions and bookmarklet link
const instructions = `
<header>
<h1>DOI Link Augmenter</h1>
<p>This is a tool designed to augment links to doi.org on a webpage. It creates two versions of the link text for each DOI link:</p>
<ul>
  <li>A version that is announced to screen readers, which can contain descriptive text like the article title.</li>
  <li>A version that is visible to sighted users, continuing to display the URL as text.</li>
</ul>
</header>
<h1>How to Use</h1>

<p><strong>Bookmarklet:</strong> Drag this link to your bookmarks bar - <a href="${bookmarkletCode}">Augment DOI links</a></p>
<p>Then click to activate it on any page containing doi.org links.</p>
<p><strong>JS File:</strong> Copy and paste the following code into your browser's console:</p>
<pre><code>
var script = document.createElement('script');
script.src = '${scriptURL}';
document.head.appendChild(script);
</code></pre>
<section id="experience-effects">
  <h2>Experiencing the Effects</h2>
  <p>To experience the effects of this tool, you can either use a screen reader or inspect the augmented DOI links using browser development tools.</p>
  
  <h3>Using VoiceOver on Mac</h3>
  <ul>
    <li>Enable VoiceOver by pressing <kbd>Cmd</kbd> + <kbd>F5</kbd>.</li>
    <li>Navigate to the DOI link using the VoiceOver rotor or arrow keys.</li>
  </ul>
  
  <h3>Using Another Screen Reader</h3>
  <p>Enable your screen reader of choice and navigate to the DOI link.</p>
  
  <h3>Using Browser Dev Tools</h3>
  <p>Right-click on the DOI link and choose 'Inspect' to view the modified link attributes in the developer tools.</p>
</section>

`;


// Delete the public directory if it exists
rimraf.sync('public');

// Create a public directory
if (!fs.existsSync('public')) {
  fs.mkdirSync('public');
}

// Write bookmarklet and instructions to public directory
fs.writeFileSync('public/index.html', instructions);
fs.copyFileSync('./main.js', './public/main.js');
// fs.writeFileSync('public/main.min.js', minifiedCode);

console.log('Build complete.');
